import OnlineStatus from '@/components/online-status'

export default function Home() {
  return (
    <main className="flex flex-col gap-4 p-8">
      <h1>Custom Hooks</h1>

      <p>This project will show your online status via a custom hook!</p>

      <OnlineStatus />
    </main>
  )
}
