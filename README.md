This project showcases how to use custom hooks in NextJS

## Getting Started

First, install dependencies, then run the development server:

```bash
bun i
```

then

```bash
bun dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Info Source

From [ReactJS Official learning documentation](https://react.dev/learn/reusing-logic-with-custom-hooks).
