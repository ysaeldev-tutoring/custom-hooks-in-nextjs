'use client'

import { useOnlineStatus } from '@/hooks/use-online-status'

export default function OnlineStatus() {
  const isOnline = useOnlineStatus() // this is a way to clean up and simplify the code

  return <p>{isOnline ? '✅ Online' : '❌ Disconnected'}</p>
}
